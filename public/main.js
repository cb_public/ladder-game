"use strict";

let MEMBERS = ["S", "H", "A", "J", "N", "W", "M", "K"];
let SPEED = 10;


var color_table = [
    'rgba(255,0,0,1)', 'rgba(0,255,0,1)', 'rgba(0,0,255,1)',
    'rgba(255,255,0,1)', 'rgba(0,255,255,1)', 'rgba(255,0,255,1)',
    'rgba(255,125,125,1)', 'rgba(30,200,30,1)', 'rgba(100,100,255,1)',
    'rgba(125,0,0,1)', 'rgba(0,80,0,1)', 'rgba(0,0,125,1)',
    'rgba(125,125,125,1)', 'rgba(80,80,80,1)', 'rgba(125,0,125,1)'
];

class Moving {
    parse_posi(v) {
        if (v[v.length-1] == 'x') {
            return parseFloat(v.substr(0, v.length-2));
        }
        return parseFloat(v);
    }

    constructor(elem, goto, animate=true) {
        if (animate) {
            let curr_posi = {
                x: this.parse_posi(elem.style.left),
                y: this.parse_posi(elem.style.top)
            };

            let player = elem.animate([
                // keyframes
                { left: elem.style.left, top: elem.style.top },
                { left: goto.x + 'px', top: goto.y + 'px' }
            ], {
                // timing options
                duration: (SPEED / 1.5)
            });

            player.onfinish = function(e) {
                elem.style.left = goto.x + 'px';
                elem.style.top = goto.y + 'px';
            };
        } else {
            elem.style.left = goto.x + 'px';
            elem.style.top = goto.y + 'px';
        }
    }
}


class ClimberList {
    constructor() {
        this.s = [];
        this.end = false;
        this.runing = 0;
    }

    push(c) {
        this.s.push(c);
        c.color = color_table[this.s.length-1];
        c.input.style.border = "4px solid " + color_table[this.s.length-1];
        c.input.value = this.s.length;
    }

    pop() {
        return this.s[this.runing ++];
    }

    update(cas) {
        let ctx = cas.getContext("2d");
        ctx.clearRect(0, 0, cas.width, cas.height);
        ctx.lineWidth = 9;

        for (let i in this.s) {
            ctx.beginPath();
            let c = this.s[i];
            let ww = i * 1;
            ctx.strokeStyle = c.color;

            for (let j in c.path) {
                let ps = c.path[j]
                let posi = c.ladder.toPosi(ps.x, ps.y);
                if (ps.d == 'l') {
                    posi = c.ladder.lineL(ctx, posi, ww);
                    new Moving(c.input, {
                        x: posi.x - ww,
                        y: posi.y + 10
                    }, j == c.path.length);
                }
                else if (ps.d == 'r') {
                    posi = c.ladder.lineR(ctx, posi, ww);
                    new Moving(c.input, {
                        x: posi.x - ww,
                        y: posi.y + 10
                    }, j == c.path.length-1);
                }
                else if (ps.d == 't') {
                    posi = c.ladder.lineT(ctx, posi, ww);
                    new Moving(c.input, {
                        x: posi.x - ww,
                        y: posi.y + 10
                    }, j == c.path.length-1);
                }
            }

            ctx.stroke();

        }
    }
}

var ccs = new ClimberList();


class Ladder {
    constructor(canvas, xnums, ynums) {
        this.size = {
            width: canvas.width,
            height: canvas.height
        };
        this.xnums = xnums;
        this.ynums = ynums;
        this.dispixel_x = (this.size.width / 2.0) / this.xnums;
        this.dispixel_y = (this.size.height / 2.0) / (this.ynums + 1.0);
        this.init(canvas.getContext('2d'));
    }

    init(context) {
        let xnums = this.xnums;
        let ynums = this.ynums;
        context.clearRect(0, 0, this.size.width, this.size.height);

        this.grids = [];
        for (let x=0; x<xnums; x++) {
            let row = [];
            this.grids.push(row);
            for (let y=0; y<=ynums; y++) {
                row.push({
                    l: false,
                    r: false,
                    t: y > 0 ? true : false,
                    b: y == ynums ? false : true
                });
            }
        }

        for (let x=0; x<xnums; x++) {
            let has_right = 0;
            let cont = 0;
            for (let y=1; y<ynums; y++) {
                let point = this.grids[x][y];
                if (x == 0) {
                    if (cont > 1) {
                        cont = 0;
                        continue;
                    }
                    if (Math.random() < 0.5) {
                        point.r = true;
                        has_right ++;
                        cont ++;
                    }
                    else {
                        cont = 0;
                    }
                }
                else {
                    if (this.grids[x-1][y].r) {
                        point.l = true;
                        cont = 0;
                    }
                    else if (x < (xnums-1)) {
                        if (cont >= 1) {
                            cont = 0;
                            continue;
                        }
                        if (Math.random() < 0.7) {
                            point.r = true;
                            has_right ++;
                            cont ++;
                        }
                        else {
                            cont = 0;
                        }
                    }
                }
            }
            if (x < (xnums-1) && has_right < 1) {
                alert("not a well laber: " + has_right + " in " + x);
                break;
            }
        }

        context.beginPath();
        context.strokeStyle = '#fff';
        for (let x=0; x<xnums; x++) {
            for (let y=0; y<=ynums; y++) {
                let point = this.grids[x][y];
                let posi = this.toPosi(x, y);
                if (point.t) {
                    this.lineT(context, posi);
                }
                if (point.b) {
                    this.lineB(context, posi);
                }
                if (point.l) {
                    this.lineL(context, posi);
                }
                if (point.r) {
                    this.lineR(context, posi);
                }
            }
        }
        context.stroke();
    }

    toPosi(x, y) {
        return {
            x: (x * this.dispixel_x * 2) + this.dispixel_x,
            y: (y * this.dispixel_y * 2) + this.dispixel_y
        }
    }

    lineL(context, posi, sf=0) {
        context.moveTo(posi.x, posi.y-sf);
        context.lineTo(posi.x - this.dispixel_x, posi.y-sf);
        return {
            x: posi.x - this.dispixel_x,
            y: posi.y
        };
    }
    lineR(context, posi, sf=0) {
        context.moveTo(posi.x, posi.y-sf);
        context.lineTo(posi.x + this.dispixel_x, posi.y-sf);
        return {
            x: posi.x + this.dispixel_x,
            y: posi.y
        };
    }
    lineB(context, posi, sf=0) {
        context.moveTo(posi.x+sf, posi.y);
        context.lineTo(posi.x+sf, posi.y + this.dispixel_y);
        return {
            x: posi.x,
            y: posi.y + this.dispixel_y
        };
    }
    lineT(context, posi, sf=0) {
        context.moveTo(posi.x+sf, posi.y);
        context.lineTo(posi.x+sf, posi.y - this.dispixel_y);
        return {
            x: posi.x,
            y: posi.y - this.dispixel_y
        };
    }
}

class Climber {
    constructor(x, y, ladder) {
        this.input = document.createElement("INPUT");
        this.input.className = "climber";
        this.input.size = 2;
        document.body.appendChild(this.input);

        this.ladder = ladder;

        let posi = this.ladder.toPosi(x, y);
        this.input.style.left = posi.x + "px";
        this.input.style.top = posi.y + "px";

        this.path = [{
            x: x,
            y: y,
            d: 0
        }];
    }

    curr_posi() {
        return this.path[this.path.length-1];
    }

    last_posi() {
        if (this.path.length > 1) {
            return this.path[this.path.length-2];
        }
        return null;
    }
}


var go_next = function(cas, climber=null) {
    let updated = false

    if (!climber) {
        climber = ccs.pop()
    }
    if (!climber) {
        return
    }

    //for (climber of ccs.s) {
        let curr_p = climber.curr_posi();
        let last_p = climber.last_posi();
        try {
            let curr_g = climber.ladder.grids[curr_p.x][curr_p.y];
            if (curr_g.l) {
                if (last_p && last_p.d == 'r') {
                    // pass
                }
                else {
                    curr_p.d = 'l';
                    climber.path.push({
                        x: curr_p.x - 0.5,
                        y: curr_p.y,
                        d: 0
                    });
                    ccs.update(cas);
                    updated = true;
                }
            }

            if (curr_g.r) {
                if (last_p && last_p.d == 'l') {
                    // pass
                }
                else {
                    curr_p.d = 'r';
                    climber.path.push({
                        x: curr_p.x + 0.5,
                        y: curr_p.y,
                        d: 0
                    });
                    ccs.update(cas);
                    updated = true;
                }
            }

            if (!updated && curr_g.t) {
                curr_p.d = 't';
                climber.path.push({
                    x: curr_p.x,
                    y: curr_p.y - 0.5,
                    d: 0
                });
                ccs.update(cas);
                updated = true;
            }
        }
        catch(e) {
            if (last_p && last_p.d == 'r') {
                curr_p.d = 'r';
                climber.path.push({
                    x: curr_p.x + 0.5,
                    y: curr_p.y,
                    d: 0
                });
                ccs.update(cas);
                updated = true;
            }
            else if (last_p && last_p.d == 'l') {
                curr_p.d = 'l';
                climber.path.push({
                    x: curr_p.x - 0.5,
                    y: curr_p.y,
                    d: 0
                });
                ccs.update(cas);
                updated = true;
            }
            else if (last_p && last_p.d == 't') {
                curr_p.d = 't';
                climber.path.push({
                    x: curr_p.x,
                    y: curr_p.y - 0.5,
                    d: 0
                });
                ccs.update(cas);
                updated = true;
            }
        }
    //}

    if (!updated) {
    }
    else {
        setTimeout(function() {
            go_next(cas, climber);
        }, SPEED);
    }
};

window.addEventListener("load", function (event) {
    let xnums = MEMBERS.length, ynums = 20;

    let canvas = document.getElementById("canvas-main");
    canvas.width = window.innerWidth - 60;
    canvas.height = window.innerHeight - 100;
    let context = canvas.getContext("2d");
    var ladder = new Ladder(canvas, xnums, ynums);

    let canvas_climber = document.getElementById("canvas-climber");
    canvas_climber.width = canvas.width;
    canvas_climber.height = canvas.height;
    let context_climber = canvas_climber.getContext("2d");

    let bn = [];
    while (bn.length < MEMBERS.length) {
        let idx = Math.round(Math.random() * MEMBERS.length);
        let mm = MEMBERS[idx];
        if (!mm) { continue }
        if (bn.indexOf(mm) < 0) {
            bn.push(mm);
        }
    }

    for (let x=0; x<xnums; x++) {
        let dd = document.createElement("div");
        dd.appendChild(document.createTextNode(bn[x]));
        dd.className = "climber";
        document.body.appendChild(dd);
        let posi = ladder.toPosi(x, 0);
        dd.style.left = (posi.x - 18) + "px";
        dd.style.top = (posi.y - 23) + "px";
    }

    for (let x=0; x<xnums; x++) {
        let cb = new Climber(x, ynums, ladder);
        ccs.push(cb);
    }

    let click_start = document.createElement("BUTTON");
    click_start.className = "start";
    document.body.appendChild(click_start);
    click_start.appendChild(document.createTextNode("Start"));

    click_start.addEventListener('click', function(e) {
        go_next(canvas_climber);
    });

    let click_reload = document.createElement("BUTTON");
    click_reload.className = "reload";
    document.body.appendChild(click_reload);
    click_reload.appendChild(document.createTextNode("Reload"));

    click_reload.addEventListener('click', function(e) {
        location.reload();
    });
});
